<?php

namespace App\Libraries;

use App\Models\Sms;
use App\Models\SmsOutcome;
use SendGrid\Mail\Mail;

class Sendgrid
{
    public function __construct()
    {
    }

    /**
     * Undocumented function
     *
     * @param string $to
     * @param string $subject
     * @param string $body
     * @return void
     */
    public function sendEmail(string $to, string $subject, string $body): void
    {
        $email = new Mail();
        $email->setFrom("no-reply@onepension.co.uk", "One Pension");
        $email->setSubject($subject);
        $email->addTo($to);
        $email->addContent("text/plain", strip_tags($body));
        $email->addContent(
            "text/html",
            $body
        );

        $sendgrid = new \SendGrid($_ENV['SENDGRID_API_KEY']);

        try {
            $response = $sendgrid->send($email);
            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";
        } catch (\Exception $e) {
            echo 'Caught exception: ' .  $e->getMessage() . "\n";
        }
    }
}
