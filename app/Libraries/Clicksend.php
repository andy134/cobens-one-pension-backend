<?php

namespace App\Libraries;

use App\Models\Sms;
use App\Models\SmsOutcome;
use Exception;

class Clicksend
{
    public function __construct()
    {
        $config = \ClickSend\Configuration::getDefaultConfiguration()
            ->setUsername($_ENV['CLICKSEND_USER'])
            ->setPassword($_ENV['CLICKSEND_PASS']);

        $this->apiInstance = new \ClickSend\Api\SMSApi(new \GuzzleHttp\Client(), $config);
    }


    public function sendSms(Sms ...$messages)
    {
        $messagesToSend = [];

        foreach ($messages as $message) {

            $messagesToSend['messages'] = [
                [
                    'source' => 'php',
                    'body' => $message->message,
                    'to' => $message->to,
                    'from' => $message->from,
                ]
            ];
        }

        $url = "https://rest.clicksend.com/v3/sms/send";
        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->post($url, [
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                'auth' => [$_ENV['CLICKSEND_USER'], $_ENV['CLICKSEND_PASS']],
                'body'    => json_encode($messagesToSend)
            ]);
        } catch (Exception $e) {
            echo 'Exception when calling sendSms: ', $e->getMessage(), PHP_EOL;
        }
    }
}
