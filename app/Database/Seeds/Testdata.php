<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Testdata extends Seeder
{
    public function run()
    {
        $this->db->query(
            "INSERT INTO applicants (uuid, answers, lastname, phone, status, created_at) VALUES('6e0cc4be-839e-4285-bab6-dd52d48deff4', '{}', 1, NOW()), ('46144914-3c06-4afe-bbe5-6332dff8a8fb', '{}', 1, NOW())"
        );
    }
}
