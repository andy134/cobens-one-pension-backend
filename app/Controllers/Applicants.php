<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use Exception;
use App\Libraries\Clicksend;
use App\Models\Sms;

class Applicants extends BaseController
{
    use ResponseTrait;

    private $json;
    private $smsObject;

    public function __construct()
    {
        $this->applicants = new \App\Models\Applicants();
        $this->json = json_decode(file_get_contents("php://input"));
        $this->clicksend = new Clicksend();

        $this->smsObject = new Sms();
        $this->smsObject->from = 'COBENS';
        $this->smsObject->message = 'Hello World';
        $this->smsObject->to = '+447795560045';
        $this->clicksend->sendSms($this->smsObject);
    }

    public function index()
    {
        return $this->respond(['error' => 'unauthorised'], 403);
    }

    public function all()
    {
        $applicants = $this->applicants->findAll();

        foreach ($applicants as &$applicant) {
            $applicant->answers = json_decode($applicant->answers);
            $applicant->questions = json_decode($applicant->questions);
            unset($applicant->id);
        }

        return $this->respond($applicants, 200);
    }

    public function create()
    {
        try {
            $applicant = $this->applicants->insert([
                'uuid' => $this->uuid(),
                'answers' => json_encode($this->json->answers),
                'questions' => json_encode($this->json->questions),
            ]);
            return $this->respondCreated($this->json, 201);
        } catch (Exception $e) {
            return $this->respond(['error' => $e->getMessage()], 500);
        }
    }

    public function applicant(string $uuid)
    {
        $applicant = $this->applicants->where('uuid', $uuid)->first();

        $applicant->answers = json_decode($applicant->answers);
        $applicant->questions = json_decode($applicant->questions);
        unset($applicant->id);
        return $this->respond($applicant, 200);
    }

    public function update(string $uuid)
    {
        try {
            $res = $this->applicants
                ->where('uuid', $uuid)
                ->set((array) $this->json)
                ->update();

            $applicant = $this->applicants->where('uuid', $uuid)->first();
            $this->smsObject->to = $applicant->phone;

            switch ($this->json->status) {
                case 2:
                    $this->smsObject->message = "Dear $applicant->firstname\n\nYour application is being processed";
                    break;
                case 3:
                    $this->smsObject->message = "Dear $applicant->firstname\n\nYour application has been approved";
                    break;
                case 4:
                    $this->smsObject->message = "Dear $applicant->firstname\n\nYour application has been rejected";
                    break;
            }

            $this->clicksend->sendSms($this->smsObject);

            return $this->respond(['updated' => $uuid], 200);
        } catch (Exception $e) {
            return $this->fail($e->getMessage(), 400);
        }
    }

    public function uuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for the time_low
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            // 16 bits for the time_mid
            mt_rand(0, 0xffff),
            // 16 bits for the time_hi,
            mt_rand(0, 0x0fff) | 0x4000,

            // 8 bits and 16 bits for the clk_seq_hi_res,
            // 8 bits for the clk_seq_low,
            mt_rand(0, 0x3fff) | 0x8000,
            // 48 bits for the node
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff)
        );
    }
}
