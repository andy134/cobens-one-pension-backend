<?php

namespace App\Models;

/**
 * Class Recording
 * @package App\Model
 * @author Andy Davies <andy@useocta.com>
 * @copyright One Call to All Limited
 * @version v2.0
 * @link https://gitlab.com/octa3/octa-asterisk
 */
class SmsOutcome implements ModelInterface
{
    /**
     * @var string|null
     */
    public ?string $messageId = null;

    /**
     * @var string|null
     */
    public ?string $outcome = null;

    public function getMessageId()
    {
        return $this->messageId;
    }
    public function setMessageId(string $messageId)
    {
        $this->messageId = $messageId;
    }

    public function getOutcome()
    {
        return $this->to;
    }
    public function setOutcome(string $outcome)
    {
        $this->outcome = $outcome;
    }
}
