<?php

namespace App\Models;

/**
 * Interface ModelInterface
 * @package Octa\Cli\Model
 * @author Andy Davies <andy@useocta.com>
 * @author lampelk <james@daveslab.co.uk>
 * @copyright One Call to All Limited
 * @version v2.0
 * @link https://gitlab.com/octa3/octa-asterisk
 */
interface ModelInterface
{
}
