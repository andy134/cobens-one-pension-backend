<?php

namespace App\Models;

/**
 * Class Recording
 * @package Octa\Cli\Model
 * @author Andy Davies <andy@useocta.com>
 * @copyright One Call to All Limited
 * @version v2.0
 * @link https://gitlab.com/octa3/octa-asterisk
 */
class Sms implements ModelInterface
{
    /**
     * @var string|null
     */
    public ?string $to = null;

    /**
     * @var string|null
     */
    public ?string $from = null;

    /**
     * @var string|null
     */
    public ?string $message = null;

    public function getTo()
    {
        return $this->to;
    }
    public function setTo(string $to)
    {
        $this->to = $to;
    }

    public function getFrom()
    {
        return $this->to;
    }
    public function setFrom(string $from)
    {
        $this->from = $from;
    }

    public function getMessage()
    {
        return $this->message;
    }
    public function setMessage(string $message)
    {
        $this->message = $message;
    }
}
