# Cobens Pension Backend

## Getting started

Make sure you have Docker desktop installed https://www.docker.com/products/docker-desktop/

Run the following commands:

    docker compose build
    docker compose up --detach
    docker exec -w /var/www/html cobens-backend-php-1 composer install
    docker exec -w /var/www/html cobens-backend-php-1 php spark migrate
    docker exec -w /var/www/html cobens-backend-php-1 php spark db:seed Testdata
